<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'         => $this->faker->word(),
            'sku'          => $this->faker->randomElement(['ADD-BLU-'.rand(1000,10000), 'GFT-BLA-'.rand(1000,10000), 'UOP-YEL-'.rand(1000,10000), 'KIA-RED-'.rand(1000,10000), 'ERA-WHI-'.rand(1000,10000), 'VER-GRE-'.rand(1000,10000)]),
            'product_code' => $this->faker->numberBetween(10000000, 999999999),
            'price'        => $this->faker->randomFloat(2, 100, 900),
            'quantity'     => $this->faker->numberBetween(10, 100),
            'color'        => $this->faker->randomElement(['BLACK', 'WHITE', 'BLUE', 'RED', 'GREEN', 'YELLOW']),
            'image'        => $this->faker->randomElement(['1614811099-1783484512.jpg', '1614811130-158951342.jpg', '1614811471-540435017.jpeg', '1614812478-754325465.jpg', '1614812506-1263137597.jpg', '1614812520-807167739.jpg']),
        ];

    }
}
