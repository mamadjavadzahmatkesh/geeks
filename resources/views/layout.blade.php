<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Geeks - @yield('title')</title>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />

        <style>
            body {
                font-family: 'Arial';
                font-size:20px;
                padding:20px;
            }
            table{
                font-size:16px;
            }
            .error{
                color:red;
                font-size:15px;
            }
            .active{
                color:black !important; 
                font-weight: bold !important;
            }
            ul{
                margin:0;padding:0;
            }
            ul>li{
                list-style: none;
            }
        </style>
        @yield('style')
    </head>
    
    <body>
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light" style="border:1px solid gray;border-radius:3px">
                <div class="container-fluid">
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link active" id="products" aria-current="page" href="{{route('products.index')}}">Products</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="searchIndex" href="{{route('products.search.index')}}">Search</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="basket" href="{{route('basket.index')}}">Final Basket</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav><br>
            @yield('content')
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                var pathname = window.location.pathname.split("/").pop();
                console.log(pathname);
                if (pathname != '/products' ) {
                    $('#products').removeClass('active');
                    $('#'+pathname).addClass('active');
                }
            });
        </script>

        @yield('script')
    </body>
</html>
