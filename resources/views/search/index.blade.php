@extends('layout')
@section('title', 'Search')
@section('style')
    <style>
        a#addPro, a#deletePro{
            color:white;
            font-size:30px;
            display: block;
            cursor: pointer;
        }
        a#addPro:hover, a#deletePro:hover{
            text-decoration:none;
            color:black;
        }
        .contSearchBtn a{
            border-radius: 3px;
            background-color: green;
        }
        .conDeletePro a{
            border-radius: 50%;
            background-color: red;
            font-size:25px!important;
            width:40px;
            height: 40px;
            text-align: center;
            margin: 0;
            position: absolute;
            top: 50%;
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
        }
        .textCon{padding-top: 5px;}
        .text-center{
            text-align: center;
        }
        .ui-autocomplete-row
        {
            padding:8px;
            background-color: #f4f4f4;
            border-bottom:1px solid #ccc;
            font-weight:bold;
            display:flex;
            align-items: center;
        }
        .ui-autocomplete-row:hover{
            background-color: #ddd;
        }
        .ui-autocomplete-row:hover img{
            background-color: transparent!important;
            padding:0px!important;
            margin:0px!important;
            margin-right:10px!important;
            border: none!important;
        } 
        .ui-autocomplete-row img{
            width: 80px; 
            height: 80px;
            padding:0px!important;
            margin:0px!important;
            margin-right:10px!important;
            border: none!important;
            position: absolute;
        }
        .ui-autocomplete-row img{
            width: 80px; 
            height: 80px;
            margin-right:10px;
            position: absolute;
        }
        .contInput{
            width: 50px!important; 
        }
        input#search_data{
            line-height: 30px;;
        }
        img#spinner{
            width:33px;
            height:33px;
        }
        #basic-addon2{
            display:none;
        }
        .basketItem{
            padding:10px;
            background-color: #d3dbd7;
            border-radius: 5px;
            text-align: center;
            margin-bottom: 10px;
        }
        .imageHolder{
            text-align: center;
            justify-content: center; 
            display: flex; 
            flex-direction: row;
            overflow: hidden;
            vertical-align: middle;
        }
        .imageHolder img{
            object-fit: cover; 
            flex: 1;
            height: 180px;
            border-radius: 5px;;
        }
        a.save{float:right}
        .nodata{text-align: center;}
    </style>
@endsection

@section('content')   
    @if(session()->get('result'))
        <div class="alert alert-info" role="alert">
            {{ session()->get('result') }}
        </div>
        <hr>
    @endif
    @if( request()->get('message') )
        <div class="alert alert-info" role="alert">
            {{ request()->get('message') }}
        </div>
        <hr>
    @endif

    <div class="jumbotron">
        <div class="row text-center">
            <div class="col-md-3 textCon">
                Search Products
            </div>
        
            <div class="col-md-6 contInput">

                <div class="input-group mb-3">
                    <input type="text" id="search_data" autocomplete="off" placeholder="Search for name/SKU/product-code" class="form-control input-lg" />
                    <div class="input-group-append2">
                        <span class="input-group-text" id="basic-addon2"><img id="spinner" src="{{asset('default/Spinner.gif')}}"></span>
                    </div>
                </div>

            </div>

            <div class="col-md-1 contSearchBtn">
                <a title="" class="" id="addPro">+</a>
            </div>
        </div>
    </div>

    <div class="jumbotron">
        @if(count($result))
            <div>Basket</div><hr>
            @foreach($result as $basketItem)
            
                <div class="row basketItem">
                    <div class="col-md-3">
                        <div class="imageHolder"><img alt="product image" src="{{ asset('uploads/productImages/'.$basketItem->product->image) }}"></div>
                    </div>
                
                    <div class="col-md-6">
                        <ul>
                            <li>Sku: {{$basketItem->product->sku}}</li>
                            <li>Name: {{$basketItem->product->name}}</li>
                            <li>Color: {{$basketItem->product->color}}</li>
                            <li>Product code: {{$basketItem->product->product_code}}</li>
                            <li>Unit Price: {{$basketItem->product->priceWithsign}}</li>
                            <li>
                                <form method="POST" action="{{route('basket.addQuantity')}}">
                                    @csrf
                                    <div class="form-group row">
                                        <label for="quantity" class=" col-md-3 offset-md-2 col-form-label text-md-right">Quantity: </label>
                                        <div class="col-md-3">
                                            <input type="number" class="form-control" name="quantity" value="{{ old('quantity') ?: $basketItem->quantity }}" min="1" max="{{$basketItem->product->quantity}}" required>
                                            @if($errors->has('name'))
                                                <div class="error">{{ $errors->first('name') }}</div>
                                            @endif
                                        </div>
                                        <input type="hidden" name="product_id" value="{{$basketItem->product->id}}">     

                                        <button type="submit" class="btn btn-info mb-3">Submit</button>
                                    </div>
                                </form>
                            </li>
                        </ul>
                    </div>

                    <div class="col-md-1 conDeletePro">
                        <a  basket-id="{{$basketItem->id}}" title="delete form basket" class="" id="deletePro">-</a>
                    </div>
                </div>

            @endforeach

            <a href="{{route('basket.index')}}" class="btn btn-primary save">Save</a>
        @else
            <div class="nodata">Basket is Empty</div>
        @endif
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function(){

            function debounce(time, fn){
                let timeoutid 
                return function(){
                    if(timeoutid){
                        clearTimeout(timeoutid)
                    }
                    timeoutid = setTimeout(()=>{
                        fn.apply(this, arguments)
                        timeoutid = null
                    }, time)
                }
            }
            
            var input = $('input#search_data');
            function selectFunction(event, ui){
                    input.val(ui.item.name)
                    input.attr('product-id',ui.item.id)
                    input.attr('product-price',ui.item.price)
                    return false;
                }
            var publicPath = "{{url('uploads/productImages')}}"
            $('#search_data').autocomplete({
                source: `/products/search`,
                minLength: 2,
                delay: 500,
                search: function(){
                    $("#basic-addon2").show()
                },
                response: function(){
                    $("#basic-addon2").hide()
                },
                select:selectFunction
            }).data('ui-autocomplete')._renderItem = function(ul, item){
                return $("<li class='ui-autocomplete-row'></li>")
                    .data("item.autocomplete", item)
                    .append(`<img src="${publicPath}/${item.image}">`)
                    .append(item.name)
                    .appendTo(ul);
            };

            $('a#addPro').click(function(){
                var productId    = $('input#search_data').attr('product-id');
                var productPrice = $('input#search_data').attr('product-price');
                
                if(productPrice == undefined || productId == undefined){
                    window.location.replace(`/products/searchIndex?message=Please select a product!`);
                }

                var request = $.ajax({
                url:`/basket/add/${productId}/${productPrice}`,
                type:"POST",
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                
                }).done(function(response){
                    window.location.replace(`/products/searchIndex?message=${response}`);
                }).catch(e=> console.log(e))
                
            });

            $('a#deletePro').click(function () {
                var basketId= $(this).attr('basket-id');
                var request = $.ajax({
                    url:`/basket/delete/${basketId}`,
                    type:"DELETE",
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },

                }).done(function(response){
                    window.location.replace(`/products/searchIndex?message=${response}`);
                }).catch(e=> console.log(e))
            });
        });
    </script>
@endsection