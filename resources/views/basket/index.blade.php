@extends('layout')
@section('title', 'Basket')
@section('style')
    <style>
        .basketItem{
            padding:10px;
            background-color: #d3dbd7;
            border-radius: 5px;
            text-align: center;
            margin-bottom: 10px;
        }
        .imageHolder{
            text-align: center;
            justify-content: center; 
            display: flex; 
            flex-direction: row;
            overflow: hidden;
            vertical-align: middle;
        }
        .imageHolder img{
            object-fit: cover; 
            flex: 1;
            height: 180px;
            border-radius: 5px;;
        }
        .nodata{text-align: center;}
        .taright{text-align: right;}
    </style>
@endsection

@section('content')   
    @if(session()->get('result'))
        <div class="alert alert-info" role="alert">
            {{ session()->get('result') }}
        </div>
        <hr>
    @endif
    @if( request()->get('message') )
        <div class="alert alert-info" role="alert">
            {{ request()->get('message') }}
        </div>
        <hr>
    @endif

    <div class="jumbotron">
        @if(count($result))
            <div class="row">
                <div class="col-md-6">Final Basket</div>
                <div class="col-md-6 taright">Basket Price = {{$BasketPrice}}</div>
            </div>
            <hr>
            @foreach($result as $basketItem)
            
                <div class="row basketItem">
                    <div class="col-md-3">
                        <div class="imageHolder"><img alt="product image" src="{{ asset('uploads/productImages/'.$basketItem->product->image) }}"></div>
                    </div>
                
                    <div class="col-md-6">
                        <ul>
                            <li>Sku: {{$basketItem->product->sku}}</li>
                            <li>Name: {{$basketItem->product->name}}</li>
                            <li>Color: {{$basketItem->product->color}}</li>
                            <li>Product code: {{$basketItem->product->product_code}}</li>
                            <li>Total Price: {{$basketItem->TotalPriceWithSign}}</li>
                            <li>Quantity: {{ $basketItem->quantity }}</li>
                        </ul>
                    </div>
                </div>

            @endforeach

        @else
            <div class="nodata">Basket is Empty</div>
        @endif
    </div>
   
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function(){

        });
    </script>
@endsection