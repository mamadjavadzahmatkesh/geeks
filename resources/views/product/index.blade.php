@extends('layout')
@section('title', 'Home')
@section('style')
    <style>
        #createButton{
            margin-bottom: 20px;
        }
        ul{
            margin:0;padding:0;
        }
        ul>li{
            list-style: none;
            display:inline-block;
        }
        td,th{
            text-align: center;
            vertical-align: middle!important;
        }
        .imageHolder{
            text-align: center;
            justify-content: center; 
            display: flex; 
            flex-direction: row;
            overflow: hidden;
        }
        .imageHolder img{
            object-fit: cover; 
            flex: 1;
            height: 100px;
        }
        a.btn-info{
            color:white;
        }
        a.btn-danger:hover{
            color:black;
        }
    </style>
@endsection

@section('content')   
    @if(session()->get('result'))
        <div class="alert alert-info" role="alert">
            {{ session()->get('result') }}
        </div>
        <hr>
    @endif
    @if( request()->get('message') )
        <div class="alert alert-info" role="alert">
            {{ request()->get('message') }}
        </div>
        <hr>
    @endif

    <a href="{{route('products.create')}}" title="create product" class="btn btn-primary" id="createButton">Create Product</a>
    
    <table class="table table-hover table-sm">
        <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col">Name</th>
                <th scope="col">SKU</th>
                <th scope="col">Code</th>
                <th scope="col">Color</th>
                <th scope="col">Quantity</th>
                <th scope="col">Price</th>
                <th scope="col">Image</th>
                <th scope="col">Created at</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @php

                if( request()->get('page') ){
                    $i = ((request()->get('page') - 1) * 5) + 1;
                }else{
                    $i = 1; 
                }
            
            @endphp

            @foreach ($result as $product)
                <tr>
                    <td>{{$i}}</td>
                    <td>{{$product->name}}</td>
                    <td>{{$product->sku}}</td>
                    <td>{{$product->product_code}}</td>
                    <td>{{$product->color}}</td>
                    <td>{{$product->quantity}}</td>
                    <td>{{$product->PriceWithSign}}</td>
                    <td><div class="imageHolder"><img alt="product image" src="{{ asset('uploads/productImages/'.$product->image) }}"></div></td>
                    <td>{{$product->created_at}}</td>
                    <td>
                        <ul>
                            <li><a href="{{route('products.edit', $product)}}" title="update product" class="btn btn-info">Update</a></li>
                            <li><button class="btn btn-danger deleteModal" data-attr="{{$product}}" data-toggle="modal" data-target="#deleteModal">Delete</button></li>
                        </ul>
                    </td>
                    @php $i++; @endphp
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $result->links() }}

    <!-- Delete Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Are you sure?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                    It will delete your product...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="modalDeleteButton" type="button" class="btn btn-danger">Delete</button>
            </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            
            $(".deleteModal").click(function () {
                var product = $(this).attr('data-attr');
                $('#modalDeleteButton').attr('data-attr', product);
            });

            $('#modalDeleteButton').click(function () {
                var product= $(this).attr('data-attr');
                let productId = JSON.parse(product).id
                var request = $.ajax({
                    url:`/products/${productId}`,
                    type:"DELETE",
                    headers: {
                        'X-CSRF-TOKEN': '{{csrf_token()}}'
                    },
                    data: {
                        "product": product
                    },
                }).done(function(response){
                    window.location.replace(`/products?message=${response}`);
                }).catch(e=> console.log(e))
            });

        });
    </script>
@endsection