@extends('layout')
@section('title', 'Edit')
@section('style')
    <style>
        .form-group{margin-bottom: 15px;}
    </style>
@endsection

@section('content')   
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Product</div>

                <div class="card-body">
                    <form method="POST" action="{{route('products.update', $product->id)}}" enctype='multipart/form-data'>
                        @csrf
                        {{ method_field('PATCH') }}
                        <div class="form-group row">
                            <label for="name" class="col-md-3 col-form-label text-md-right">Names</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="name" value="{{ old('name') ?: $product->name }}" name="name" required>
                                @if($errors->has('name'))
                                    <div class="error">{{ $errors->first('name') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="color" class="col-md-3 col-form-label text-md-right">Color</label>
                            <div class="col-md-9">
                                <select class="custom-select" name="color" id="color" aria-label="" required>
                                    <option 
                                        @if(old('color'))
                                        {{ ( old('color') == 'RED' ) ? 'selected' : '' }}
                                        @else
                                        {{ ( $product->color == 'Red' ) ? 'selected' : '' }}
                                        @endif
                                    value="RED" >Red</option>

                                    <option
                                        @if(old('color'))
                                        {{ ( old('color') == 'GREEN' ) ? 'selected' : '' }}
                                        @else
                                        {{ ( $product->color == 'Green' ) ? 'selected' : '' }}
                                        @endif
                                    value="GREEN">Green</option>

                                    <option 
                                        @if(old('color'))
                                        {{ ( old('color') == 'BLACK' ) ? 'selected' : '' }}
                                        @else
                                        {{ ( $product->color == 'Black' ) ? 'selected' : '' }}
                                        @endif                                   
                                    value="BLACK">Black</option>

                                    <option
                                        @if(old('color'))
                                        {{ ( old('color') == 'BLUE' ) ? 'selected' : '' }}
                                        @else
                                        {{ ( $product->color == 'Blue' ) ? 'selected' : '' }}
                                        @endif                                     
                                    value="BLUE" >Blue</option>

                                    <option
                                        @if(old('color'))
                                        {{ ( old('color') == 'YELLOW' ) ? 'selected' : '' }}
                                        @else
                                        {{ ( $product->color == 'Yellow' ) ? 'selected' : '' }}
                                        @endif                                      
                                    value="YELLOW">Yellow</option>

                                    <option 
                                        @if(old('color'))
                                        {{ ( old('color') == 'WHITE' ) ? 'selected' : '' }}
                                        @else
                                        {{ ( $product->color == 'White' ) ? 'selected' : '' }}
                                        @endif                                     
                                    value="WHITE">White</option>
                                </select>
                                @if($errors->has('color'))
                                    <div class="error">{{ $errors->first('color') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="product_code" class="col-md-3 col-form-label text-md-right">Product code</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="product_code" value="{{ old('product_code') ?: $product->product_code }}" name="product_code" required>
                                @if($errors->has('product_code'))
                                    <div class="error">{{ $errors->first('product_code') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="quantity" class="col-md-3 col-form-label text-md-right">Quantity</label>
                            <div class="col-md-9">
                                <input type="number" class="form-control" id="quantity" value="{{ old('quantity') ?: $product->quantity }}" name="quantity" min="1" required>
                                @if($errors->has('quantity'))
                                    <div class="error">{{ $errors->first('quantity') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="price" class="col-md-3 col-form-label text-md-right">Price</label>
                            <div class="col-md-9">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" id="price" value="{{ old('price') ?: $product->price }}" name="price"  aria-describedby="basic-addon2" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon2">$</span>
                                    </div>
                                </div>
                                @if($errors->has('price'))
                                    <div class="error">{{ $errors->first('price') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="image" class="col-md-3 col-form-label text-md-right">Image</label>
                            <div class="col-md-9">
                                <input type="file" class="form-control" id="image" value="{{ old('image') ?: $product->image }}" name="image">
                                @if($errors->has('image'))
                                    <div class="error">{{ $errors->first('image') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary mb-3">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
