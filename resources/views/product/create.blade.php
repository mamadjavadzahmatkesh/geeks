@extends('layout')
@section('title', 'Create')
@section('style')
    <style>
        .form-group{margin-bottom: 15px;}
    </style>
@endsection

@section('content')   
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Product</div>

                <div class="card-body">
                    <form method="POST" action="{{route('products.store')}}" enctype='multipart/form-data'>
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-3 col-form-label text-md-right">Names</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="name" value="{{ old('name') }}" name="name" required>
                                @if($errors->has('name'))
                                    <div class="error">{{ $errors->first('name') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="color" class="col-md-3 col-form-label text-md-right">Color</label>
                            <div class="col-md-9">
                                <select class="custom-select" name="color" id="color" aria-label="" required>
                                    <option value="RED" {{ old('color') == 'RED' ? 'selected' : '' }}>Red</option>
                                    <option value="GREEN" {{ old('color') == 'GREEN' ? 'selected' : '' }}>Green</option>
                                    <option value="BLACK" {{ old('color') == 'BLACK' ? 'selected' : '' }}>Black</option>
                                    <option value="BLUE" {{ old('color') == 'BLUE' ? 'selected' : '' }}>Blue</option>
                                    <option value="YELLOW" {{ old('color') == 'YELLOW' ? 'selected' : '' }}>Yellow</option>
                                    <option value="WHITE" {{ old('color') == 'WHITE' ? 'selected' : '' }}>White</option>
                                </select>
                                @if($errors->has('color'))
                                    <div class="error">{{ $errors->first('color') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="product_code" class="col-md-3 col-form-label text-md-right">Product code</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="product_code" value="{{ old('product_code') }}" name="product_code" required>
                                @if($errors->has('product_code'))
                                    <div class="error">{{ $errors->first('product_code') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="quantity" class="col-md-3 col-form-label text-md-right">Quantity</label>
                            <div class="col-md-9">
                                <input type="number" class="form-control" id="quantity" value="{{ old('quantity') }}" name="quantity" min="1" required>
                                @if($errors->has('quantity'))
                                    <div class="error">{{ $errors->first('quantity') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="price" class="col-md-3 col-form-label text-md-right">Price</label>
                            <div class="col-md-9">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" id="price" value="{{ old('price') }}" name="price" aria-describedby="basic-addon2" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon2">$</span>
                                    </div>
                                </div>
                                @if($errors->has('price'))
                                    <div class="error">{{ $errors->first('price') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="image" class="col-md-3 col-form-label text-md-right">Image</label>
                            <div class="col-md-9">
                                <input type="file" class="form-control" id="image" value="{{ old('image') }}" name="image" required>
                                @if($errors->has('image'))
                                    <div class="error">{{ $errors->first('image') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary mb-3">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
