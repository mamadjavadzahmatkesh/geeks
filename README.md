# Project Title
Code challenge for Geeks ltd company.

# Project Description
According to the code challenge scenario, all mandatory and some optional quests are implemented. First of all, you can generate some fake products by seeder. To follow SOLID and Clean code principles, each functionality of controllers is placed in separate services, also Eloquent ORM is used to build our queries by repository design pattern.

The CRUD operations are implemented to manage products entity. Each product has a unique SKU and Product code generated by mixing the product's attributes. On the search page, you can search products by name or SKU, or product code. Also, the debounce method is implemented to decrease the number of requests to the server when a user types in the search input. Change the quantity of each product and remove methods is implemented too. The final basket placed on the basket page and the Total price of the whole basket calculated.

# Getting started

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/8.x)

Clone the repository

    git clone https://gitlab.com/mamadjavadzahmatkesh/geeks.git

Switch to the repo folder and install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Run the database migrations (*Set the database connection configuration in .env before migrating*)

    php artisan migrate

Run the database seed to generate fake products

    php artisan db:seed

Start the local development server

    php artisan serve

Now you can access the project at http://localhost:8000/products

## Built With
* php 7.4.3
* mysql 5
* laravel v8.0
