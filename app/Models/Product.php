<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    //Relation
    public function basket()
    {
        return $this->hasOne(Basket::class);
    }

    // Accessors
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('M d Y, H:i');
    }

    public function getPriceWithSignAttribute($value)
    {
        return $this->attributes['PriceWithSign'] = $this->price." $";
    }

    public function getColorAttribute($value)
    {
        $value = strtolower($value);
        return ucwords($value);
    }
}
