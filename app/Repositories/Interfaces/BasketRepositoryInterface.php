<?php

namespace App\Repositories\Interfaces;

use App\Models\Product;
use Illuminate\Http\Request;

interface BasketRepositoryInterface
{
    public function getAll();

    public function add($id, $price);

    public function checkDuplicateProduct($id);

    public function addQuantity(Request $request);

    public function deleteByProductId($product_id);

    public function delete($id);

    public function BasketPrice();

    public function UpdateTotalPrice(Product $product);
}
