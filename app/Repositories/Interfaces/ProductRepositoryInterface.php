<?php

namespace App\Repositories\Interfaces;

use App\Models\Product;
use Illuminate\Http\Request;

interface ProductRepositoryInterface
{
    public function getAll();

    public function store(Request $request, $image);

    public function insertSku(Product $product, $sku);

    public function update(Request $request, Product $product, $newImage);

    public function delete(Product $product);

    public function search(Request $request);

}
