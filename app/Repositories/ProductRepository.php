<?php

namespace App\Repositories;

use App\Models\Product;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use Illuminate\Http\Request;

class ProductRepository implements ProductRepositoryInterface
{
    public function getAll()
    {
        $products = Product::orderBy('created_at', 'desc')->paginate(5);

        return $products;
    }

    public function store(Request $request, $image)
    {
        $product               = new Product();
        $product->name         = $request->name;
        $product->color        = $request->color;
        $product->product_code = $request->product_code;
        $product->quantity     = $request->quantity;
        $product->price        = $request->price;
        $product->image        = $image;
        $product->save();

        return $product;
    }

    public function insertSku(Product $product, $sku)
    {
        $product->sku = $sku;
        $product->save();

        return $product;
    }

    public function update(Request $request, Product $product, $newImage)
    {
        $product->name         = $request->name; 
        $product->color        = $request->color; 
        $product->product_code = $request->product_code; 
        $product->quantity     = $request->quantity; 
        $product->price        = $request->price; 

        if($newImage){
            $product->image = $newImage; 
        }

        $product->save();

        return $product;
    }

    public function delete(Product $product)
    {
        $deleted = $product->delete();

        return $deleted ? 'Product successfully deleted.' : 'Delete operation encountered a problem!';
    }

    public function search(Request $request)
    {
        $products = Product::where('name', 'like', '%'.$request->term.'%')
        ->orWhere('sku', 'like', '%'.$request->term.'%')
        ->orWhere('product_code', 'like', '%'.$request->term.'%')
        ->take(10)->get();

        return $products;
    }
}
