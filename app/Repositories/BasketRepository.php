<?php

namespace App\Repositories;

use App\Models\Basket;
use App\Models\Product;
use App\Repositories\Interfaces\BasketRepositoryInterface;
use Illuminate\Http\Request;

class BasketRepository implements BasketRepositoryInterface
{
    public function getAll()
    {
        $basket = Basket::with('product')->orderBy('created_at', 'desc')->get();

        return $basket;
    }

    public function add($id, $price)
    {
        $basket = new Basket();
        $basket->product_id = $id;
        $basket->quantity   = 1;
        $basket->TotalPrice = $price;
        $basket->save();

        return $basket;
    }

    public function checkDuplicateProduct($id)
    {
        $basket = Basket::where('product_id',$id)->first();

        return $basket ? true : false;
    }

    public function addQuantity(Request $request)
    {
        $product = Product::find($request->product_id);
        
        if($request->quantity > $product->quantity){
            return 'Quantity for product '.$product->name.' is invalid.';
        }

        $basket             = Basket::where('product_id',$request->product_id)->first();
        $basket->quantity   = $request->quantity;
        $basket->TotalPrice = $request->quantity * $product->price;
        $basket->save();
        
        return 'Quantity for product '.$product->name.' applied.';
    }

    public function deleteByProductId($product_id)
    {
        $basketItem = Basket::where('product_id', $product_id)->first();
        
        if($basketItem instanceof Basket){
            $basketItem->delete();    
        }
    }

    public function delete($id)
    {
        $basketItem = Basket::find($id);
        
        $deleted = $basketItem->delete(); 
        
        return $deleted ? 'Product successfully deleted from basket.' : 'Delete operation encountered a problem!';
    }

    public function BasketPrice()
    {        
        $total = Basket::get()->sum('TotalPrice');
            
        return $total;
    }

    public function UpdateTotalPrice(Product $product)
    {        
        $basket = Basket::where('product_id', $product->id)->first();

        if($basket){
            $newTotalPrice      = $product->price * $basket->quantity;
            $basket->TotalPrice = $newTotalPrice;
            $basket->save();
        }
    }
}
