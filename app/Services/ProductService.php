<?php

namespace App\Services;

use App\Models\Product;
use App\Repositories\Interfaces\BasketRepositoryInterface;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use App\Services\Interfaces\ProductServiceInterface;
use Illuminate\Http\Request;

class ProductService implements ProductServiceInterface
{
    private $ProductRepository;
    private $BasketRepository;

    public function __construct(ProductRepositoryInterface $ProductRepository, BasketRepositoryInterface $BasketRepository)
    {
        $this->ProductRepository = $ProductRepository;
        $this->BasketRepository  = $BasketRepository;
    }

    public function getAll()
    {
        $products = $this->ProductRepository->getAll();

        return $products;
    }

    public function store(Request $request)
    {
        $image = $this->imageUploader($request);

        $product = $this->ProductRepository->store($request, $image);

        $sku = $this->GenerateSku($product);

        $product = $this->ProductRepository->insertSku($product, $sku);

        return $product;
    }

    public function imageUploader(Request $request)
    {
        $imageName = time() . '-' . rand();
        $imageType = $request->file('image')->getClientOriginalExtension();
        $request->file('image')->move(public_path('uploads/productImages'), $imageName . '.' . $imageType);

        return $imageName . '.' . $imageType;
    }

    public function GenerateSku(Product $product)
    {
        return strtoupper(substr($product->name, 0, 3)).'-'.strtoupper(substr($product->color, 0, 3)).'-'.$product->id;         
    }

    public function update(Request $request, Product $product)
    {
        $newImage = null; 
        if($request->image){
            $newImage = $this->imageUploader($request);
        }

        $product = $this->ProductRepository->update($request, $product, $newImage);

        $sku = $this->GenerateSku($product);

        $product = $this->ProductRepository->insertSku($product, $sku);

        return $product;
    }

    public function delete(Product $product)
    {
        $message = $this->ProductRepository->delete($product);
        $this->BasketRepository->deleteByProductId($product->id);

        return $message;
    }

    public function search(Request $request)
    {
        $results = $this->ProductRepository->search($request);

        return $results;
    }
}
