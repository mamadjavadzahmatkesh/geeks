<?php

namespace App\Services;

use App\Models\Basket;
use App\Models\Product;
use App\Repositories\Interfaces\BasketRepositoryInterface;
use App\Services\Interfaces\BasketServiceInterface;
use Illuminate\Http\Request;

class BasketService implements BasketServiceInterface
{
    private $BasketRepository;

    public function __construct(BasketRepositoryInterface $BasketRepository)
    {
        $this->BasketRepository = $BasketRepository;
    }

    public function getAll()
    {
        $basket = $this->BasketRepository->getAll();

        return $basket;
    }

    public function add($id, $price)
    {
        $checkDuplicateProduct = $this->BasketRepository->checkDuplicateProduct($id);

        if($checkDuplicateProduct === true){
            return $message = 'Product already added! Please try another one.';
        }

        $basket = $this->BasketRepository->add($id, $price);

        if ($basket instanceof Basket) {
            $message = 'Product successfully added to basket.';
        } else {
            $message = 'Adding to basket encountered a problem!';
        }

        return $message;
    }

    public function addQuantity(Request $request)
    {
        $message = $this->BasketRepository->addQuantity($request);

        return $message;
    }

    public function delete($id)
    {
        $message = $this->BasketRepository->delete($id);

        return $message;
    }

    public function BasketPrice()
    {
        $total = $this->BasketRepository->BasketPrice();

        return $total." $";
    }

    public function UpdateTotalPrice(Product $product)
    {
        $this->BasketRepository->UpdateTotalPrice($product);

    }
}
