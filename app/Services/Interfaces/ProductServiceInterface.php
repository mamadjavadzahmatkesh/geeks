<?php

namespace App\Services\Interfaces;

use App\Models\Product;
use Illuminate\Http\Request;

interface ProductServiceInterface
{
    public function getAll();

    public function store(Request $request);

    public function imageUploader(Request $request);

    public function GenerateSku(Product $product);

    public function update(Request $request, Product $product);

    public function delete(Product $product);

    public function search(Request $request);

}
