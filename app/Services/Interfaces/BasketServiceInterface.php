<?php

namespace App\Services\Interfaces;

use App\Models\Product;
use Illuminate\Http\Request;

interface BasketServiceInterface
{
    public function getAll();

    public function add($id, $price);

    public function addQuantity(Request $request);

    public function delete($id);

    public function BasketPrice();

    public function UpdateTotalPrice(Product $product);

}
