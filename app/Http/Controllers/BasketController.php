<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\BasketServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class BasketController extends Controller
{
    private $BasketService;

    public function __construct(BasketServiceInterface $BasketService)
    {
        $this->BasketService = $BasketService;
    }

    /**
     * Show the all items in basket.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        $basket = $this->BasketService->getAll();
        
        if(count($basket) > 0)
        {
            $BasketPrice = $this->BasketService->BasketPrice();
        }else{
            $BasketPrice = "0.00 $";
        }

        return view('basket.index', ['result' => $basket, 'BasketPrice' => $BasketPrice]);
    }

    /**
     * Add a product to basket.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function add($id, $price)
    {
        $message = $this->BasketService->add($id, $price);

        return $message;
    }

    /**
     * Add quantity of a product in basket.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addQuantity(Request $request)
    {
        $valid = array(
            'product_id' => 'required|exists:products,id,deleted_at,NULL',
            'quantity'   => 'required|numeric|min:1',
        );

        $validator = Validator::make($request->all(), $valid);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $message = $this->BasketService->addQuantity($request);

        return redirect()->route('products.search.index')->with(['result' => $message]);
    }

    /**
     * Delete a product from basket.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $message = $this->BasketService->delete($id);

        return $message;
    }
}
