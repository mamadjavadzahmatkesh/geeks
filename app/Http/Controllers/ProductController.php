<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Services\Interfaces\ProductServiceInterface;
use App\Services\Interfaces\BasketServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class ProductController extends Controller
{
    public $successStatusCode = 200;
    private $ProductService;
    private $BasketService;

    public function __construct(ProductServiceInterface $ProductService, BasketServiceInterface $BasketService)
    {
        $this->ProductService = $ProductService;
        $this->BasketService  = $BasketService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->ProductService->getAll();

        return view('product.index', ['result' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valid = array(
            'name'         => 'required|max:250',
            'product_code' => 'required|max:250|unique:products,product_code',
            'quantity'     => 'required|numeric|min:1',
            'price'        => 'required|numeric|between:0,99999999',
            'color'        => 'required|in:BLACK,BLUE,RED,GREEN,YELLOW,WHITE',
            'image'        => 'required|image|mimes:jpeg,png,jpg|max:'.config('app.ProductImageSize'),
        );

        $validator = Validator::make($request->all(), $valid);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        // Insert new product to database
        $product = $this->ProductService->store($request);

        if ($product instanceof Product) {
            $message = 'Product successfully created.';
        } else {
            $message = 'Product creation encountered a problem!';
        }

        return redirect()->route('products.index')->with(['result' => $message]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('product.edit')->with(['product' => $product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $valid = array(
            'name'         => 'required|max:250',
            'product_code' => 'required|max:250|unique:products,product_code,'.$product->id,
            'quantity'     => 'required|numeric|min:1',
            'price'        => 'required|numeric|between:0,99999999',
            'color'        => 'required|in:BLACK,BLUE,RED,GREEN,YELLOW,WHITE',
            'image'        => 'sometimes|image|mimes:jpeg,png,jpg|max:'.config('app.ProductImageSize'),
        );

        $validator = Validator::make($request->all(), $valid);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        // update product to database
        $product = $this->ProductService->update($request, $product);

        // update TotalPrice of basket
        $this->BasketService->UpdateTotalPrice($product);

        if ($product instanceof Product) {
            $message = 'Product successfully updated.';
        } else {
            $message = 'Product update encountered a problem!';
        }

        return redirect()->route('products.index')->with(['result' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $message = $this->ProductService->delete($product);

        return $message;
    }

     /**
     * Show the product search page
     *
     * @return \Illuminate\Http\Response
     */
    public function searchIndex()
    {
        $basket = $this->BasketService->getAll();

        return view('search.index',['result' => $basket]);
    }

    /**
     * Search by product name or sku or code
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $results = $this->ProductService->search($request);

        return response()->json($results, $this->successStatusCode,);
    }
}
