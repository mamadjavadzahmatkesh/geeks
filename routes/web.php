<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\BasketController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'products'], function () {
    Route::get('/searchIndex', [ProductController::class, 'searchIndex'])->name('products.search.index');
    Route::get('/search', [ProductController::class, 'search'])->name('products.search');
});

Route::resource('products', ProductController::class);

Route::group(['prefix' => 'basket'], function () {
    Route::get('/', [BasketController::class, 'getAll'])->name('basket.index');
    Route::post('/add/{id}/{price}', [BasketController::class, 'add'])->name('basket.add');
    Route::delete('/delete/{id}', [BasketController::class, 'delete'])->name('basket.delete');
    Route::post('/addQuantity', [BasketController::class, 'addQuantity'])->name('basket.addQuantity');
});


